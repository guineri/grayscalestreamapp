package org.parallelme.samples.grayscale;

import android.graphics.Bitmap;

import java.util.Objects;
import java.util.Vector;
import java.util.concurrent.Semaphore;

public interface GrayscaleOperator {
    void grayscale(Bitmap bitmapIn, Bitmap bitmapOut, Object lock, int exec);
    void waitFinish();
}
