package org.parallelme.samples.grayscale;

import android.content.Context;
import android.graphics.Bitmap;

import java.util.Vector;
import java.util.concurrent.Semaphore;

public class GrayscaleNativeCalls implements GrayscaleOperator {
    private long dataPointer;

    private native long nativeInit();
    private native void nativeCleanUp(long dataPointer);
    private native void nativeGrayscale(long dataPointer, Bitmap bitmapIn, Bitmap bitmapOut, int width, int height, Object lock, int exec);
    private native void nativeWaitFinish(long dataPointer);

    @Override

    public void grayscale(Bitmap imageIn, Bitmap imageOut, Object lock, int exec) {
        nativeGrayscale(dataPointer, imageIn, imageOut, imageIn.getWidth(), imageIn.getHeight(),lock, exec);
    }

    public void waitFinish(){
        nativeWaitFinish(dataPointer);
    }

     public boolean inited() {
        return dataPointer != 0;
    }

    GrayscaleNativeCalls() {
        dataPointer = nativeInit();
    }

    protected void finalize() throws Throwable {
        try {
            if(dataPointer != 0)
                nativeCleanUp(dataPointer);
        } catch(Throwable t) {
            throw t;
        } finally {
            super.finalize();
        }
    }

    static {
        System.loadLibrary("GrayscaleStream");
    }
}