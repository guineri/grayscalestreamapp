package org.parallelme.samples.grayscale;
import android.app.Activity;
import android.app.ActivityManager;
import android.os.AsyncTask;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Objects;
import java.util.Vector;
import java.util.concurrent.Semaphore;

public class MainActivity extends Activity {
    private GrayscaleOperator operator;
    private ImageView imageView;
    private Bitmap mBitmap;
    private Bitmap image;
    private int mNumImages;
    private TextView mBenchmarkText;
    private EditText imagesQnt;
    private EditText mStreamTime;
    private CheckBox mNetCheck;
    private CheckBox mUpsCheck;
    private Boolean networkers;
    private Boolean upworkers;
    long streamTime;
    private GrayscaleNativeCalls grayscaleNative;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        imageView = (ImageView) findViewById(R.id.image_view);
        mBenchmarkText = (TextView) findViewById(R.id.bench);
        imagesQnt = (EditText) findViewById(R.id.numImages);
        mStreamTime = (EditText) findViewById(R.id.stream_time);
        mNetCheck = (CheckBox) findViewById(R.id.networkers_check);
        mUpsCheck = (CheckBox) findViewById(R.id.ups_check);
        grayscaleNative = new GrayscaleNativeCalls();

        upworkers = false;
        networkers = false;

        image = BitmapFactory.decodeResource(getResources(), R.drawable.rainbow);

        imagesQnt.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {

                if (imagesQnt.getText().toString().compareTo("") != 0) {
                    mNumImages = Integer.parseInt(imagesQnt.getText().toString());
                } else {
                    mNumImages = 0;
                }

            }
        });

        mStreamTime.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {

                if (mStreamTime.getText().toString().compareTo("") != 0) {
                    streamTime = Integer.parseInt(mStreamTime.getText().toString());
                    streamTime = streamTime*1000; //s to ms
                } else {
                    streamTime = 0;
                }

            }
        });
    }

    public void onCheckBoxClicked(View v){

        boolean checked = ((CheckBox) v).isChecked();

        switch(v.getId()){
            case R.id.networkers_check:
                if (checked){
                    networkers = true;
                }
                else{
                   networkers = false;
                }
                break;
            case R.id.ups_check:
                if (checked){
                    upworkers = true;
                }
                else{
                    upworkers = false;
                }
                break;
        }
    }

    /** Called when the user clicks the Grayscale button */
    public void grayscaleStart(View view) throws Exception {
        //mProgressDialog.show();
        new GrayscaleStreamTask().execute(grayscaleNative);
    }

    public void showOriginal(View view) {
        Bitmap image = BitmapFactory.decodeResource(getResources(), R.drawable.rainbow);
        imageView.setImageBitmap(image);
    }

    public void showGrayscale(View view) {
        new GrayscaleStreamTask().execute(grayscaleNative);
    }


    private class GrayscaleStreamTask extends AsyncTask<GrayscaleNativeCalls, Void, Bitmap> {
        long mTime;
        long tasksExecuted;

        private final Semaphore available = new Semaphore(1);
        private Object lock = new Object();

        protected Boolean bitmapEmpty(Bitmap out, Bitmap empty){

            boolean ret = false;

            synchronized (lock){
                 ret = out.sameAs(empty);
            }

            return ret;
        }
        @Override
        protected Bitmap doInBackground(GrayscaleNativeCalls... grayscaleOP) {
            long time;

            Vector<Bitmap> bitmapsIn = new Vector<>();
            Vector<Bitmap> bitmapsOut = new Vector<>();
            Vector<Boolean> bitmapsSubmited = new Vector<>();

            Bitmap emptyBitmap = Bitmap.createBitmap(image.getWidth(), image.getHeight(),Bitmap.Config.ARGB_8888);
            System.out.println("[DEBUG] NUM IMAGES: " + mNumImages + "--- " + image.getWidth() + "/" + image.getHeight());
            for(int i = 0; i < mNumImages; ++i) {
                bitmapsOut.add(Bitmap.createBitmap(image.getWidth(), image.getHeight(),Bitmap.Config.ARGB_8888));
                bitmapsIn.add(image);
                bitmapsSubmited.add(false);
            }

            //for(int i = 0; i < mNumImages; ++i)
            //    grayscaleOP[0].grayscale(bitmapsIn.get(i),bitmapsOut.get(i),lock);
            //grayscaleOP[0].waitFinish();
            //mTime = (java.lang.System.currentTimeMillis() - time);

            int execution = 2   ;
            if(upworkers && networkers) execution = 1;          //Scheduled extern and inter
            else if(upworkers && !networkers) execution = 2;    //Just Inter
            else if (!upworkers && networkers) execution = 3;   //Just extern

            time = java.lang.System.currentTimeMillis();
            tasksExecuted = 0;
            while(streamTime > mTime){
                for(int i = 0; i < mNumImages; ++i) {
                    if (bitmapEmpty(bitmapsOut.get(i),emptyBitmap)) {
                        if(!bitmapsSubmited.get(i)) {
                            grayscaleOP[0].grayscale(bitmapsIn.get(i), bitmapsOut.get(i),lock,execution);
                            tasksExecuted++;
                            bitmapsSubmited.add(i, true);
                            //System.out.println("Submetendo bitmap: " + i);
                        }
                        else{
                           // System.out.println("Bitmap " + i + " está executando!");
                        }
                    }
                    else {
                        //System.out.println("Bitmap: " + i + " finalizado!");
                        bitmapsOut.get(i).recycle();
                        bitmapsOut.removeElementAt(i);
                        bitmapsOut.add(i, Bitmap.createBitmap(image.getWidth(), image.getHeight(), Bitmap.Config.ARGB_8888));
                        //bitmapsSubmited.add(i,false);
                        System.out.println("TasksExecuted: " + tasksExecuted);
                        grayscaleOP[0].grayscale(bitmapsIn.get(i), bitmapsOut.get(i),lock,execution);
                        tasksExecuted++;
                    }
                }
                mTime = (java.lang.System.currentTimeMillis() - time);
            }
            grayscaleOP[0].waitFinish();
            Bitmap returnBitmap = Bitmap.createScaledBitmap(bitmapsOut.lastElement(),
                                                            bitmapsOut.lastElement().getWidth(),
                                                            bitmapsOut.lastElement().getHeight(),
                                                            false);
            for(int i = 0; i < mNumImages; i++){
                bitmapsOut.get(i).recycle();
            }

            bitmapsIn.clear();
            bitmapsOut.clear();
            return returnBitmap;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            // mBitmap and mBenchmarkText can only be modified in the UI thread.
            mBitmap = bitmap;
            imageView.setImageBitmap(mBitmap);

            String result = "Task Time: " + (mTime / tasksExecuted) + "ms";
            if(mNumImages > 1){
                result += " (mean)\nTotal Time: " + mTime + "ms";
            }
            result += "\nExecuted Tasks: " + tasksExecuted;
            result += "\nThroughput: " + (tasksExecuted / (mTime/1000)) + " task/s";


            mBenchmarkText.setText(result);
            //mProgressDialog.dismiss();
        }

    }
}
