#include <parallelus/ParallelUS.hpp>
#include <android/log.h>
#include <stddef.h>
#include "org_parallelme_samples_grayscale_GrayscaleNativeCalls.h"

using namespace parallelus;

const static char gKernels[] =
    "__kernel void grayscale(__global uchar4 *image) { \n"
    "    int gid = get_global_id(0);                   \n"
    "    uchar4 pixel = image[gid];                    \n"
    "                                                  \n"
    "    uchar luminosity = 0.21f * pixel.x            \n"
    "        + 0.72f * pixel.y + 0.07f * pixel.z;      \n"
    "    pixel.x = pixel.y = pixel.z = luminosity;     \n"
    "                                                  \n"
    "    image[gid] = pixel;                           \n"
    "}                                                 \n";

struct NativeData {
    std::shared_ptr<Runtime> runtime;
    std::shared_ptr<Program> program;   
};

JNIEXPORT jlong JNICALL Java_org_parallelme_samples_grayscale_GrayscaleNativeCalls_nativeInit
        (JNIEnv *env, jobject self) {

    JavaVM *jvm;
    env->GetJavaVM(&jvm);
    if(!jvm) return (jlong) nullptr;

    auto dataPointer = new NativeData();
    dataPointer->runtime = std::make_shared<Runtime>(jvm,true);
    dataPointer->program = std::make_shared<Program>(dataPointer->runtime, gKernels);
    return (jlong) dataPointer;
}

JNIEXPORT void JNICALL Java_org_parallelme_samples_grayscale_GrayscaleNativeCalls_nativeCleanUp
        (JNIEnv *env, jobject self, jlong dataLong) {
    auto dataPointer = (NativeData *) dataLong;
    delete dataPointer;
}
JNIEXPORT void JNICALL Java_org_parallelme_samples_grayscale_GrayscaleNativeCalls_nativeGrayscale
        (JNIEnv *env, jobject self, jlong dataLong, jobject bitmapIn, jobject bitmapOut, jint width, jint height, jobject lock, jint exec) {

    /* get semaphore ref */

    auto dataPointer = (NativeData *) dataLong;
    auto imageSize = width * height;
    auto outputBitmap = env->NewGlobalRef(bitmapOut);
    auto lock1 = env->NewGlobalRef(lock);
    auto bitmapBuffer = std::make_shared<Buffer>(Buffer::sizeGenerator(imageSize, Buffer::RGBA));
    bitmapBuffer->setAndroidBitmapSource(env, bitmapIn);

    auto task = std::make_unique<Task>(dataPointer->program);

    task->addKernel("grayscale");

	task->setConfigFunction([=] (DevicePtr &device, KernelHash &kernelHash, unsigned type) {

    kernelHash["grayscale"]
		->setArg(0, bitmapBuffer, type)
		->setWorkSize(imageSize,1,1,type);
    });

	task->setFinishFunction([=] (DevicePtr &device, KernelHash &kernelHash, unsigned type){
        
        if(type) bitmapBuffer->ExternCopyToAndroidBitmap(device->JNIEnv(), outputBitmap,lock1);
        else{
            device->JNIEnv()->MonitorEnter(lock1);
            bitmapBuffer->copyToAndroidBitmap(device->JNIEnv(), outputBitmap);
            device->JNIEnv()->MonitorExit(lock1);    
        }
    });

    dataPointer->runtime->submitTask(std::move(task), exec);

    //dataPointer->runtime->finish(); //Create an extern funcion nativeWaitFinish
    
    //bitmapBuffer->copyToAndroidBitmap(env, bitmap);
}

JNIEXPORT void JNICALL Java_org_parallelme_samples_grayscale_GrayscaleNativeCalls_nativeWaitFinish
        (JNIEnv *env, jobject self, jlong dataLong) {
	auto dataPointer = (NativeData *) dataLong;
	dataPointer->runtime->finish();
}
